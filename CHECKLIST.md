# Recommendation checklist

Below is a proposed checklist of recommendations toward best practices for machine learning with fNIRS.

## Methodology
- [ ] Is the generalisation goal adequate with the experimental design of the data collection (eg. no order effect)?
- [ ] Is the training, validation, and test set split adequate with the generalisation goal (eg. unseen subject, unseen session)?
- [ ] Has nested cross-validation been used, with the outer cross-validation (leaving out the test sets) for evaluation and the inner cross-validation (leaving out the validation sets) for hyperparameter selection?
- [ ] Has hyperparameter selection been performed and has it been done only on the validation sets?
- [ ] Have the test sets been used for evaluation only (eg. no hyperparameter selection performed on the test sets)?
- [ ] Has it been ensured that test sets were not included when performing normalisation?
- [ ] Has it been ensured that there was no overlap between training, validation, and test sets (especially when epoching with sliding windows)?
- [ ] Have the appropriate metrics been used (eg. accuracy, precision, recall)?
- [ ] Has a statistical analysis been performed to demonstrate the significance of the results compared to chance level and other models?

## Reporting
- [ ] Have the number of classes and chance level clearly been stated?
- [ ] Have the inputs of the models been described (eg. input shape, number of examples per class)?
- [ ] Has the cross-validation implementation been described (including data shuffling if used)?
- [ ] Has hyperparameter selection been described (eg. selection method, hyperparameter ranges)?
- [ ] Have the details of each model been provided (eg. architecture, hyperparameters)?
- [ ] Has the statistical analysis of the results been described (eg. name of the tests, verification of assumptions, p-values)?
