Sliding window benchmarking
===========================

Below is a comparison of 4 machine learning models (LDA, SVC, kNN, ANN) on the 5 datasets with a subject-independent approach (testing with unseen subjects), with a 2-second sliding window on the epochs to split the data into more examples [#benerradi2023]_.

.. literalinclude:: ../../../examples/sliding_window.py


.. rubric:: References
.. [#benerradi2023] Benerradi, J., Clos, J., Landowska, A., Valstar, M. F., & Wilson, M. L. (2023). Benchmarking framework for machine learning classification from fNIRS data. Frontiers in Neuroergonomics, 4, 994969.
