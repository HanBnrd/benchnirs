Custom model training
=====================

Below is an example of how to use `BenchNIRS` to train a custom convolutional neural network (CNN) on one of the datasets.

.. literalinclude:: ../../../examples/tailored_shin_nb.py
