Epochs visualisation
====================

Below is a visualisation the epochs from the 5 datasets using the MNE backend [#gramfort2013]_.

.. literalinclude:: ../../../examples/visualisation.py


.. rubric:: References
.. [#gramfort2013] Gramfort, A., Luessi, M., Larson, E., Engemann, D. A., Strohmeier, D., Brodbeck, C., ... & Hämäläinen, M. (2013). MEG and EEG data analysis with MNE-Python. Frontiers in neuroscience, 7, 70133.
