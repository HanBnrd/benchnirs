.. BenchNIRS documentation master file

BenchNIRS
=========

.. image:: https://hanbnrd.gitlab.io/assets/img/logos/benchnirs.png
  :target: https://gitlab.com/HanBnrd/benchnirs
  :width: 110
  :height: 110
  :align: right
  :alt: BenchNIRS

`Benchmarking framework for machine learning with fNIRS`

Features:

* loading of open access datasets
* signal processing and feature extraction on fNIRS data
* training, hyperparameter tuning and evaluation of machine learning models (including deep learning)
* production of training graphs, metrics and other useful figures for evaluation
* benchmarking and comparison of machine learning models
* supervised, self-supervised and transfer learning
* much more!

.. role::  raw-html(raw)
    :format: html

:raw-html:`&rarr;` `Source code on GitLab <https://gitlab.com/HanBnrd/benchnirs>`_

.. image:: https://img.shields.io/pypi/v/benchnirs
  :target: https://pypi.org/project/benchnirs/

.. image:: https://img.shields.io/badge/license-MIT-blue
  :target: https://gitlab.com/HanBnrd/benchnirs/-/blob/main/LICENSE

.. image:: https://img.shields.io/badge/doi-10.3389%2Ffnrgo.2023.994969-blue
  :target: https://doi.org/10.3389/fnrgo.2023.994969

.. toctree::
   :maxdepth: 1
   :caption: Contents

   install
   modules
   examples


Recommendation checklist
------------------------

.. |uncheck| raw:: html

    <input type="checkbox" disabled="">

.. |check| raw:: html

    <input checked="" type="checkbox" disabled="">

Below is a proposed checklist of recommendations toward best practices for machine learning with fNIRS (`BenchNIRS` provides an API for the checked items).

| **Methodology**:
| |uncheck| Is the generalisation goal adequate with the experimental design of the data collection (eg. no order effect)?
| |check| Is the training, validation, and test set split adequate with the generalisation goal (eg. unseen subject, unseen session)?
| |check| Has nested cross-validation been used, with the outer cross-validation (leaving out the test sets) for evaluation and the inner cross-validation (leaving out the validation sets) for hyperparameter selection?
| |check| Has hyperparameter selection been performed and has it been done only on the validation sets?
| |check| Have the test sets been used for evaluation only (eg. no hyperparameter selection performed on the test sets)?
| |check| Has it been ensured that test sets were not included when performing normalisation?
| |check| Has it been ensured that there was no overlap between training, validation, and test sets (especially when epoching with sliding windows)?
| |uncheck| Have the appropriate metrics been used (eg. accuracy, precision, recall)?
| |uncheck| Has a statistical analysis been performed to demonstrate the significance of the results compared to chance level and other models?

| **Reporting**:
| |uncheck| Have the number of classes and chance level clearly been stated?
| |uncheck| Have the inputs of the models been described (eg. input shape, number of examples per class)?
| |uncheck| Has the cross-validation implementation been described (including data shuffling if used)?
| |uncheck| Has hyperparameter selection been described (eg. selection method, hyperparameter ranges)?
| |uncheck| Have the details of each model been provided (eg. architecture, hyperparameters)?
| |uncheck| Has the statistical analysis of the results been described (eg. name of the tests, verification of assumptions, p-values)?


Contributing to the repository
------------------------------

Contributions from the community to this repository are highly appreciated. We are mainly interested in contributions to:

* improving the recommendation checklist
* adding support for new open access datasets
* adding support for new machine learning models
* adding more fNIRS signal processing techniques
* improving the documentation
* tracking bugs

Contributions are encouraged under the form of `issues <https://gitlab.com/HanBnrd/benchnirs/-/issues>`_ (for reporting bugs or requesting new features) and `merge requests <https://gitlab.com/HanBnrd/benchnirs/-/merge_requests>`_ (for fixing bugs and implementing new features).
Please refer to `this tutorial <https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html>`_ for creating merge requests from a fork of the repository.


Contributors
------------

* Johann Benerradi
* Jeremie Clos
* Aleksandra Landowska
* Michel F. Valstar
* Max L. Wilson
* Yujie Yao


Acknowledgements
----------------

This project is licensed under the `MIT License <https://gitlab.com/HanBnrd/benchnirs/-/blob/main/LICENSE>`_, if you are using `BenchNIRS` please cite `this article <https://doi.org/10.3389/fnrgo.2023.994969>`_.


Indices and tables
------------------

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
