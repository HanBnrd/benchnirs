Examples
========

.. toctree::
   :maxdepth: 2

   examples/simple-example
   examples/custom-model
   examples/generalised
   examples/personalised
   examples/sliding-window
   examples/window-size
   examples/dataset-size
   examples/visualisation
