BenchNIRS API
=============

Data loading
------------

.. autofunction:: benchnirs.load_dataset

.. autofunction:: benchnirs.load_homer


Visualisation 
-------------

.. autofunction:: benchnirs.epochs_viz


Data processing
---------------

.. autofunction:: benchnirs.process_epochs

.. autofunction:: benchnirs.extract_features


Machine learning
----------------

.. autofunction:: benchnirs.machine_learn

.. autofunction:: benchnirs.deep_learn

.. autofunction:: benchnirs.train_final
