Install
=======

Setting up BenchNIRS
--------------------

#. Download and install Python 3.9 or greater, for example with `Miniconda <https://docs.conda.io/projects/miniconda/en/latest/index.html>`_.

#. To install the package with `pip` (cf. `PyPI <https://pypi.org/project/benchnirs/>`_), open a terminal (eg. Anaconda Prompt) and type:

    .. code-block:: console

        pip install benchnirs

#. Download the datasets (see below).

.. note::
    Alternatively to install from source in development mode, download and unzip the `repository <https://gitlab.com/HanBnrd/benchnirs/-/archive/main/benchnirs-main.zip>`_ (or clone it with Git), and run :code:`devinstall.py`.


Downloading the datasets
------------------------
* `Herff et al. 2014 <http://www.csl.uni-bremen.de/CorpusData/download.php?crps=fNIRS>`_ (n-back task)
* `Shin et al. 2018 <http://doc.ml.tu-berlin.de/simultaneous_EEG_NIRS/NIRS/NIRS_01-26_MATLAB.zip>`_ (n-back and word generation tasks)
* `Shin et al. 2016 <http://doc.ml.tu-berlin.de/hBCI>`_ (mental arithmetic task, select `NIRS_01-29` for the fNIRS data)
* `Bak et al. 2019 <https://figshare.com/ndownloader/files/18069143>`_ (motor execution task)


Enabling GPU support
--------------------
To enable GPU acceleration on `BenchNIRS` with PyTorch (if your machine has an NVIDIA GPU), open a terminal (eg. Anaconda Prompt) and type:

.. code-block:: console

   pip install --upgrade torch --index-url https://download.pytorch.org/whl/cu118

For more information please see the `PyTorch documentation <https://pytorch.org/get-started/locally/>`_.


Keeping BenchNIRS up to date
----------------------------

To update `BenchNIRS` to the latest version with `pip`, open a terminal (eg. Anaconda Prompt) and type:

.. code-block:: console

   pip install --upgrade benchnirs
