import numpy as np
import pandas as pd
from scipy import stats


CONFIDENCE = 0.05  # stat confidence at 95 %


new_results = '../results_new_model.csv'
old_results = '../results_old_model.csv'

# Stats
print('Stats...')
df_new = pd.read_csv(new_results, delimiter=';')
df_old = pd.read_csv(old_results, delimiter=';')

print("-----\nshin_2018_nb")
sub_df_new = df_new[
    (df_new['dataset'] == 'shin_2018_nb')]
accuracies_new = sub_df_new['accuracy'].to_numpy()
sub_df_old = df_old[
    (df_old['dataset'] == 'shin_2018_nb')]
accuracies_old = sub_df_old['accuracy'].to_numpy()

# Check normality and homogeneity of variances
_, p_shap_new = stats.shapiro(accuracies_new)
_, p_shap_old = stats.shapiro(accuracies_old)
_, p_bart = stats.bartlett(accuracies_new, accuracies_old)
if all([p_shap_new > CONFIDENCE,
        p_shap_old > CONFIDENCE,
        p_bart > CONFIDENCE]):
    # t-test
    if np.mean(accuracies_new) >= np.mean(accuracies_old):
        s_tt, p_tt = stats.ttest_ind(accuracies_new, accuracies_old,
                                     alternative='greater')
        print(f't-test | new > old | {p_tt}')
    else:
        s_tt, p_tt = stats.ttest_ind(accuracies_old, accuracies_new,
                                     alternative='greater')
        print(f't-test | old > new | {p_tt}')
else:
    # Wilcoxon
    if np.mean(accuracies_new) >= np.mean(accuracies_old):
        s_mwu, p_mwu = stats.mannwhitneyu(accuracies_new, accuracies_old,
                                          alternative='greater')
        print(f'Mann-Whitney U | new > old | {p_mwu}')
    else:
        s_mwu, p_mwu = stats.mannwhitneyu(accuracies_old, accuracies_new,
                                          alternative='greater')
        print(f'Mann-Whitney U | old > new | {p_mwu}')
